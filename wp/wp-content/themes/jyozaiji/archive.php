<?php
/**
 * The template for displaying archive pages
 *
 * Used to display archive-type pages if nothing more specific matches a query.
 * For example, puts together date-based pages if no date.php file exists.
 *
 * If you'd like to further customize these archive views, you may create a
 * new template file for each one. For example, tag.php (Tag archives),
 * category.php (Category archives), author.php (Author archives), etc.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 */

get_header(); ?>

	<div id="contents">
		<div class="news">
		<h1><img src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/top/tl_news.png" alt="お知らせ"></h1>
		<?php if ( have_posts() ) : ?>

		<ul class="list">

			<?php
			// Start the Loop.
			while ( have_posts() ) : the_post();

				/*
				 * Include the Post-Format-specific template for the content.
				 * If you want to override this in a child theme, then include a file
				 * called content-___.php (where ___ is the Post Format name) and that will be used instead.
				 */
				get_template_part( 'content', get_post_format() );

			// End the loop.
			endwhile;
		?>

		</ul>
			<?php
		else :
			get_template_part( 'content', 'none' );

		endif;
		?>
		</div>
		<ul class="news-paging cinzel">
			<?php
			// Previous/next page navigation.
			the_posts_pagination( array(
				'prev_next'          => false,
				'before_page_number' => '<li>',
				'after_page_number' => '</li>',
			) );
		?>
		</ul>
	</div><!--/#contents -->


<?php get_footer(); ?>
