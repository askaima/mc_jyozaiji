<?php
/**
 * The default template for displaying content for magazine.
 *
 * Used for both single and index/archive/search.
 *
 */
?>

<?php
$terms = get_the_terms( get_the_ID(), 'magazine_cat' );
if ( !empty($terms) ) {
	if ( !is_wp_error( $terms ) ) {
		foreach( $terms as $term ) {
			$magazine_name = "[" . $term->name . "] ";
		}
	}
}
?>

<?php
	if ( is_single()) :
?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
<div class="post">
  <div class="title">
	<span class="date cinzel"><?php echo get_post_modified_time('F,d,Y'); ?></span>
	<?php
			the_title( '<h2 class="entry-title">' . $magazine_name, '</h2>' );
	?>
  </div>

	<div class="entry-content">

	<?php 
	if(has_post_thumbnail()) {
		the_post_thumbnail(array(600,400));
	} else {
		echo '<p><img src="'. get_template_directory_uri() .'/images/magazine/no_images.jpg" /></p>';
	}
	?>
		<?php
			/* translators: %s: Name of current post */
			the_content( sprintf(
				__( 'Continue reading %s', 'twentyfifteen' ),
				the_title( '<span class="screen-reader-text">', '</span>', false )
			) );

			wp_link_pages( array(
				'before'      => '<div class="page-links"><span class="page-links-title">' . __( 'Pages:', 'twentyfifteen' ) . '</span>',
				'after'       => '</div>',
				'link_before' => '<span>',
				'link_after'  => '</span>',
				'pagelink'    => '<span class="screen-reader-text">' . __( 'Page', 'twentyfifteen' ) . ' </span>%',
				'separator'   => '<span class="screen-reader-text">, </span>',
			) );
		?>
	</div><!-- .entry-content -->
</div>

</article><!-- #post-## -->


<?php
	else :
?>

	<?php
		if (isFirst()) :
	?>
	<div class="recommend">
		<p><a href="<?php the_permalink(); ?>">
	<?php 
	if(has_post_thumbnail()) {
		the_post_thumbnail(array(600,400));
	} else {
		echo '<p><img src="'. get_template_directory_uri() .'/images/magazine/no_images.jpg" /></p>';
	}
	?></p>
		<p class="date cinzel"><?php echo get_post_time('F,d,Y'); ?></p>
		<p class="tl"><a href="<?php the_permalink(); ?>"><?php echo $magazine_name; ?><?php the_title(); ?></a></p>
		<p><?php the_excerpt(); ?></p>
	</div>

	<?php
		else :
	?>
	<li>
	<?php 
	if(has_post_thumbnail()) {
		the_post_thumbnail(array(300,200));
	} else {
		echo '<p class="img"><img src="'. get_template_directory_uri() .'/images/magazine/no_images.jpg" /></p>';
	}
	?>
	
		<p class="date cinzel"><?php echo get_post_time('F,d,Y'); ?></p>
		<p class="tl"><a href="<?php the_permalink(); ?>" rel="bookmark">
			<?php echo $magazine_name; ?><?php the_title(); ?>
		</a>
		</p>
	</li>
	<?php
		endif;
	?>

<?php
	endif;
?>
