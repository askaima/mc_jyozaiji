<?php
/**
 * The template for displaying the header
 *
 * Displays all of the head element and everything up until the "site-content" div.
 *
 * @package WordPress
 * @subpackage Twenty_Fifteen
 * @since Twenty Fifteen 1.0
 */
?><!DOCTYPE html>
<html <?php language_attributes(); ?> class="no-js">
<head>
  <meta charset="<?php bloginfo( 'charset' ); ?>">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title><?php if ( is_home() ) { bloginfo('description');echo " | ";} else { wp_title('|',true,'right');} ?><?php bloginfo('name'); ?></title>
<?php if ( is_home() ) {
	$description="東京・世田谷でお墓をお探しの方へ、常在寺の納骨堂・永代供養墓。資料のご請求、ご見学を受け付けています。";
} 
	else if (is_category()) {
	$description=category_description();
}
	else if (is_page()||is_single()) { 
	$description=get_the_excerpt();
}
	else { 
	$description="";
}
	if ( $paged >= 2 || $page >= 2) {
	$description="";
}
/***delete<p></p>***/
	$description=str_replace("<p>", "", $description);
	$description=str_replace("</p>", "", $description);
/***delete<p></p>end***/
	$keywords="世田谷,墓,葬式,納骨堂,永代供養墓,常在寺,じょうざいじ,見学,東京";
?>
<?php if ( is_single()) {
}else {
	echo "  <meta name=\"keywords\" content=\"" . $keywords . "\" />\n";
	echo "  <meta name=\"description\" content=\"" . $description . "\" />\n";} 
?>
  <link rel="profile" href="http://gmpg.org/xfn/11">
  <link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
  <!--[if lt IE 9]>
  <script src="<?php echo esc_url( get_template_directory_uri() ); ?>/js/html5.js"></script>
  <![endif]-->
  <script>(function(){document.documentElement.className='js'})();</script>
  <?php wp_head(); ?>
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
  <script src="<?php echo esc_url( get_template_directory_uri() ); ?>/common/js/libs/jquery.easing.js"></script>
  <script src="<?php echo esc_url( get_template_directory_uri() ); ?>/common/js/libs/jquery.page-scroller.js"></script>
  <link href='//fonts.googleapis.com/css?family=Cinzel' rel='stylesheet' type='text/css'>
  <?php
    if ( is_front_page() || is_home() ) : ?>
  <script src="http://maps.googleapis.com/maps/api/js?v=3.exp"></script>
  <script src="<?php echo esc_url( get_template_directory_uri() ); ?>/common/js/jyozaiji.top.js"></script>
  <?php else : ?>
  <?php endif; ?>
<script type="text/javascript">

</script>

</head>

  <?php
    if ( is_front_page() && is_home() ) : ?>

<body <?php body_class(); ?>>
<div class="introduction">
	<div class="inner">
		<p><a href="javascript:void(0);" class="link01"><img src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/top/link_main_know.png" alt="知る"></a></p>
		<p><a href="javascript:void(0);" class="link02"><img src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/top/link_main_read.png" alt="読む"></a></p>
		<h1><img src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/top/logo_main.png" alt="世田谷 常在寺" width="211" height="259"></h1>
		<p><a href="javascript:void(0);" class="link03"><img src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/top/link_main_look.png" alt="見る"></a></p>
		<p><a href="javascript:void(0);" class="link04"><img src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/top/link_main_visit.png" alt="訪う"></a></p>
		<p class="arrow"><a href="javascript:void(0);" class="link05">次へ</a></p>
	</div>
</div>
    <?php endif; ?>
<div id="header" class="intro">
	<p class="logo"><a href="/"><img src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/common/logo.png" alt="世田谷 常在寺" width="183" height="40"></a></p>
		<?php wp_nav_menu( array(
			'theme_location'=>'headermenu', 
			'container'     =>'', 
			'menu_class'    =>'ttl',
			'items_wrap'    =>'<ul>%3$s</ul>'));
		?>
</div>
<!--  /ヘッダー   -->