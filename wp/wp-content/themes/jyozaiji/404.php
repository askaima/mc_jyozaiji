<?php
/**
 * The template for displaying 404 pages (not found)
 *
 */

get_header(); ?>

<div id="page" class="<?php echo get_post($wp_query->post->ID)->post_name; ?>">
<div id="contents" class="magazine">
	<h1><a href="magazine"><img src="/wp/wp-content/themes/jyozaiji/images/magazine/tl_magazine.png" alt="常在寺だより"></a></h1>
			<section>
				<ul class="sort">
					<?php wp_nav_menu( array(
						'theme_location'=>'archivemenu', 
						'container'     =>'', 
						'menu_class'    =>'',
						'items_wrap'    =>'<ul>%3$s</ul>'));
					?>
				</ul>
			</section>

		
<div class="recommend">
<p class="center" style="font-size: 16px;">現在、該当の記事はありません。<br />
申し訳ありませんが、別のカテゴリーをお選びください。</p>
<p class="" style="padding: 10px 0; font-size: 16px"><a href="/magazine/">常在寺だより</a>に戻る</p>
</div>
</div>

<?php get_footer(); ?>
