<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * e.g., it puts together the home page when no home.php file exists.
 *
 * Learn more: {@link https://codex.wordpress.org/Template_Hierarchy}
 *
 */

get_header(); ?>
		<!--   メインビジュアル   -->
		<div id="top_visual">
			<div class="news_box">
				<dl>
				<?php
					$postslist = get_posts('numberposts=5&orderby=post_date&order=DESC"');
					foreach ($postslist as $post) : setup_postdata($post);
				?>
				<?php endforeach; ?>
				</dl>
			</div>
		</div><!--  /メインビジュアル   -->

		<!--   コンテンツ   -->
<div id="contents" class="top">
	<h2 id="know"><img src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/top/tl_know.png" alt="知る" width="47" height="96"></h2>
	<ul class="know">
		<li class="box01">
			<h3><img src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/top/sub_tl_know01.png" alt="常在寺の縁起" width="247" height="39"></h3>
			<p>常在寺は、山梨県の身延山久遠寺を総本山とする<br>日蓮宗の寺院です。</p>
			<p><a href="/engi/" class="btnWht">詳細へ</a></p>
		</li>
		<li class="box02">
			<p class="sub"><img src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/top/txt_en.png" alt="縁の会" width="75" height="23"></p>
			<h3><img src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/top/sub_tl_know02.png" alt="永代供養" width="165" height="38"></h3>
			<p>常在寺は、山梨県の身延山久遠寺を総本山とする<br>日蓮宗の寺院です。</p>
			<p><a href="/eitaikuyou/" class="btnWht">詳細へ</a></p>
		</li>
		<li class="box03">
			<h3><img src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/top/sub_tl_know03.png" alt="庭園葬" width="122" height="38"></h3>
			<p>常在寺は、山梨県の身延山久遠寺を総本山とする<br>日蓮宗の寺院です。</p>
			<p><a href="/teiensou/" class="btnWht">詳細へ</a></p>
		</li>
		<li class="box04">
			<h3><img src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/top/sub_tl_know04.png" alt="動物葬" width="122" height="38"></h3>
			<p>常在寺は、山梨県の身延山久遠寺を総本山とする<br>日蓮宗の寺院です。</p>
			<p><a href="/doubutsusou/" class="btnWht">詳細へ</a></p>
		</li>
	</ul>

	<h2 id="read"><img src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/top/tl_read.png" alt="読む" width="47" height="96"></h2>
	<div class="read">
		<h3><img src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/top/sub_tl_read.png" alt="常在寺だより" width="232" height="39"></h3>
		<dl>
				<?php
					$postslist_mag = get_posts('post_type=magazine&numberposts=2&orderby=post_date&order=DESC"');
					foreach ($postslist_mag as $post) : setup_postdata($post);
				?>
				<dd class="cinzel"><?php echo get_post_time('F,d,Y');  ?> UPDATED<dd><dt><a href="<?php the_permalink();?>"><?php the_title(); ?></a></dt>
				<?php endforeach; ?>
		</dl>
		<p><a href="/magazine/" class="btnWht">記事一覧へ</a></p>
	</div>

	<h2 id="look"><img src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/top/tl_look.png" alt="見る" width="46" height="95"></h2>
	<div class="look">
		<h3><img src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/top/sub_tl_look.png" alt="常在寺 施設案内" width="274" height="34"></h3>
		<p>四季折々、表情豊かな常在寺の施設をご案内いたします。</p>
		<p><a href="/facility/" class="btnWht">常在寺 施設案内へ</a></p>
	</div>

	<h2 id="visit"><img src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/top/tl_visit.png" alt="訪う" width="47" height="99"></h2>
	<div class="visit">
		<h3><img src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/top/sub_tl_visit.png" alt="常在寺 遠野" width="201" height="34"></h3>
		<p>遠野の宿泊施設「常在寺 遠野」で、時の流れに思いを馳せる。</p>
		<p><a class="btnWht">Coming Soon</a></p>
<!-- 2015/4/27
		<p><a href="" class="btnWht">常在寺 遠野詳細へ</a></p>
-->
	</div>

	<div class="news">
		<h2><img src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/top/tl_news.png" alt="お知らせ" width="167" height="40"></h2>
		<ul class="gothic">
				<?php
					$postslist = get_posts('post_type=news&numberposts=5&orderby=post_date&order=DESC"');
					foreach ($postslist as $post) : setup_postdata($post);
				?>
				<li><?php the_time('Y.m.d');  ?><br /><a href="<?php the_permalink();?>"><?php the_title(); ?></a></li>
				<?php endforeach; ?>
		</ul>
		<p><a href="/news/" class="btnBlk">お知らせをすべて見る</a></p>
	</div>

	<div id="access" class="access">
		<h2><img src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/top/tl_access.png" alt="交通アクセス" width="232" height="39"></h2>
		<div id="map"></div>
		<div class="fl gothic">
			<h4>世田谷 常在寺</h4>
			<p class="mb30">〒154-0016 東京都世田谷区弦巻1-34-17<br>常在寺事務局 TEL 03-5450-7588</p>
			<dl class="train">
				<dt>電車</dt>
				<dd>東急世田谷線「世田谷駅」下車　徒歩7分</dd>
			</dl>
			<dl class="car">
				<dt>お車</dt>
				<dd>国道246号または世田谷通りからすぐ<br><span class="note">※30台収容の無料駐車場を完備しております。</span><br>タクシーの場合は、東急田園都市線「桜新町駅」より約5分</dd>
			</dl>
			<dl class="bus">
				<dt>バス</dt>
				<dd>
					<p>渋谷駅西口ターミナルより</p>
					<ul>
						<li>弦巻営業所行き（渋05）「向天神橋」下車　徒歩5分</li>
						<li>上町行き（渋21）、祖師谷大蔵行き（渋23）、<br>成城学園駅西口行き（渋24）<br>ともに「世田谷区役所入口」下車徒歩5分</li>
					</ul>
				</dd>
			</dl>
		</div>
		<p class="fr"><img src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/top/img_map.png" alt="地図" width="530" height="463"></p>

		<div class="transportation gothic">
			<div class="inner">
				<h3><img src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/top/tl_transportation.png" alt="世田谷エリア限定 無料送迎タクシー運行中" width="576" height="29"></h3>
				<p>ご自宅とお寺を往復。世田谷地域の皆さまを送迎サービス付きで見学会へご案内いたします。<br>おひとりでも、ご夫婦やご家族ご一緒でも結構です。（一組四名様まで）<br>ご自宅、またはご自宅近くのご指定の場所を往復いたします。お電話にてご予約ください。</p>
				<p class="tl"><img src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/top/sub_tl_transportation01.png" alt="タクシー送迎でのご案内の日時" width="840" height="40"></p>
				<dl class="date">
					<dt>1月5日（月）から2月15日（日）までの毎日 / 一日3回 / 各回一組</dt>
					<dd class="time">1. 午前10時～11時</dd>
					<dd class="time">2. 午後1時～2時</dd>
					<dd class="time">3. 午後3時～4時</dd>
					<dd>＊完全予約制ですので、ご希望の日時をご指定ください。<br>＊上記日時以外をご希望の場合はご相談ください。</dd>
				</dl>

				<div class="taxi">
					<p class="tl"><img src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/top/sub_tl_transportation02.png" alt="お迎えから見学、お送りまでの流れ" width="840" height="40"></p>
					<ul class="flow">
						<li>
							<dl>
								<dt><img src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/top/txt_flow01.png" alt="お迎え" width="83" height="25"></dt>
								<dd>上記、ご見学予定のお時間の30分前にご自宅またはご指定の場所まで、km国際タクシーでお迎えに参ります。</dd>
							</dl>
						</li>
						<li>
							<dl>
								<dt><img src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/top/txt_flow02.png" alt="見学" width="58" height="27"></dt>
								<dd>常在寺に到着後、お寺の中をご案内し、永代供養墓についてご説明いたします。<br>（所要時間約40分）</dd>
							</dl>
						</li>
						<li>
							<dl>
								<dt><img src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/top/txt_flow03.png" alt="お送り" width="73" height="26"></dt>
								<dd>終了後、ご自宅またはご指定の場所までお送りします。</dd>
							</dl>
						</li>
					</ul>
				</div>
			</div>
		</div>
	</div>

<?php get_footer(); ?>
