<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the "site-content" div and all content after.
 *
 */
?>
        </div><!-- /.container -->
    </div><!-- /#page -->
  </div><!-- /#container -->

<div id="contact" class="contact">
	<h2><img src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/common/tl_contact.png" alt="お問い合わせ・資料請求・見学予約" width="555" height="35"></h2>
	<p class="t24">常在寺 事務局</p>
	<p class="tel cinzel">03-5450-7588</p>
	<p class="gothic t14">受付時間 午前9時～午後5時［年中無休］</p>
	<p class="fax cinzel">FAX 03-5450-6559</p>
	<p class="gothic t16">見学会開催予定　2015年 5月22日（金）・28日（木）</p>
	<p><a href="/contact/" class="btnBlk">インターネットからのお問い合わせ・資料請求はこちら</a></p>
</div>

<div id="footer" class="gothic">
	<div class="info">
	<h2>世田谷 常在寺</h2>
		<?php wp_nav_menu( array(
			'theme_location'=>'footermenu', 
			'container'     =>'', 
			'menu_class'    => 'link'));
		?>
	<p class="copyright cinzel">&copy; JYOZAIJI HOURIN-NO-KAI All Rights Reserved</p>
	</div>
</div>


<p id="pagetop"><a href="javascript:void(0)" onclick="$('html,body').animate({ scrollTop: 0 }, 1000, 'easeOutQuart');">ページトップへ</a></p>
<p class="temp" style="display:none;"><a href="javascript:void(0);" class="link06">access</a></p>
<?php wp_footer(); ?>
</body>
</html>
