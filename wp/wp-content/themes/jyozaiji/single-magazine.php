<?php
/**
 * The template for displaying all single posts and attachments
 *
 */

get_header(); ?>

	<div id="contents" class="magazine-detail">

	<h1><a href="/magazine"><img src="/wp/wp-content/themes/jyozaiji/images/magazine/tl_magazine.png" alt="常在寺だより"></a></h1>
	<ul class="sort">
		<?php wp_nav_menu( array(
			'theme_location'=>'archivemenu', 
			'container'     =>'', 
			'menu_class'    => '',
			'items_wrap'    =>'<ul>%3$s</ul>'));
		?>
	</ul>
		<?php
		// Start the loop.
		while ( have_posts() ) : the_post();

			/*
			 * Include the post format-specific template for the content. If you want to
			 * use this in a child theme, then include a file called called content-___.php
			 * (where ___ is the post format) and that will be used instead.
			 */
			get_template_part( 'content', 'magazine' );

			// If comments are open or we have at least one comment, load up the comment template.
			if ( comments_open() || get_comments_number() ) :
				comments_template();
			endif;
		?>
	<ul class="pages">
<?php $previous = get_previous_post(); $next = get_next_post(); ?>
		<li class="before">
		<p class="preview">＜ 前へ</p>
		<p><?php echo get_the_post_thumbnail($previous->ID, 'thumbnail'); ?></p>
		<p class="date cinzel"><?php echo  get_post_time('F,d,Y',false ,$previous->ID); ?></p>
		<p class="tl"><a title="<?php echo esc_attr($previous->post_title); ?>" href="<?php echo get_permalink($previous->ID); ?>"><?php echo esc_attr($previous->post_title); ?></a></p>
		</li>
		<li class="after">
		<p class="next">次へ ＞</p>
		<p><?php echo get_the_post_thumbnail($next->ID, 'thumbnail'); ?></p>
		<p class="date cinzel"><?php echo  get_post_time('F,d,Y', false,$next->ID); ?></p>
		<p class="tl"><a title="<?php echo esc_attr($next->post_title); ?>" href="<?php echo get_permalink($next->ID); ?>"><?php echo esc_attr($previous->post_title); ?></a></p>
		</li>
	</ul>
		<?php
		// End the loop.
		endwhile;
		?>

	</div><!--/#contents -->

<?php get_footer(); ?>
