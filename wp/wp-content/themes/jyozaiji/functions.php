<?php
/**
 * JYOZAIJI functions and definitions
 *
 * Set up the theme and provides some helper functions, which are used in the
 * theme as custom template tags. Others are attached to action and filter
 * hooks in WordPress to change core functionality.
 *
 * When using a child theme you can override certain functions (those wrapped
 * in a function_exists() call) by defining them first in your child theme's
 * functions.php file. The child theme's functions.php file is included before
 * the parent theme's file, so the child theme functions would be used.
 *
 * @link https://codex.wordpress.org/Theme_Development
 * @link https://codex.wordpress.org/Child_Themes
 *
 * Functions that are not pluggable (not wrapped in function_exists()) are
 * instead attached to a filter or action hook.
 *
 * For more information on hooks, actions, and filters,
 * {@link https://codex.wordpress.org/Plugin_API}
 *
 */

//スタイルシート
function newStylesheet_URI () {
    $stylesheet_URI = "<link rel='stylesheet' href='" . get_stylesheet_uri() . "' type='text/css' media='all' />\n";
    echo $stylesheet_URI;
}
add_action('wp_head', 'newStylesheet_URI' );


//サムネイル機能
if ( function_exists( 'add_theme_support' ) ) { 
  add_theme_support( 'post-thumbnails' ); 
}

//カスタムメニュー
register_nav_menus( array(
	'headermenu'   => __( 'グローバルナビメニュー'),
	'archivemenu'   => __( 'アーカイブメニュー'),
	'footermenu'   => __( 'フッターメニュー'),
) );


//投稿タイプ追加
function my_custom_init() {
    register_post_type( 'magazine', array(
        'label' => '常在寺だより',
        'public' => true,
        'supports' => array( 'title', 'editor', 'author', 'thumbnail', 'excerpt', 'custom-fields' ,'comments' ),
        'menu_position' => 5,
        'has_archive' => true
    ));

  register_taxonomy(
    'magazine_cat', 
    'magazine', 
    array(
      'hierarchical' => true, 
      'update_count_callback' => '_update_post_term_count',
      'label' => 'だよりカテゴリ',
      'singular_label' => 'だよりカテゴリ',
      'public' => true,
      'show_ui' => true
    )
  );

    register_post_type( 'news', array(
        'label' => 'お知らせ',
        'public' => true,
        'supports' => array( 'title', 'editor', 'author', 'thumbnail', 'excerpt', 'custom-fields' ,'comments' ),
        'menu_position' => 5,
        'has_archive' => true
    ));

  register_taxonomy(
    'news_cat', 
    'news', 
    array(
      'hierarchical' => true, 
      'update_count_callback' => '_update_post_term_count',
      'label' => 'お知らせカテゴリ',
      'singular_label' => 'お知らせカテゴリ',
      'public' => true,
      'show_ui' => true
    )
  );

}
add_action( 'init', 'my_custom_init' );

//1件目の記事取得
function isFirst(){
    global $wp_query;
    return ($wp_query->current_post === 0);
}

//管理画面カスタム投稿一覧の並び順
function show_term_mag( $defaults ) {
$defaults['magazine_cat'] = 'だよりカテゴリ';
return $defaults;
}
add_filter('manage_magazine_posts_columns', 'show_term_mag', 15, 1);

function show_term_mag_id($column_name, $id) {
	if( $column_name == 'magazine_cat' ) {
		$terms = get_the_terms( $id, 'magazine_cat' );
		$cnt = 0;
		if($terms) {
			foreach($terms as $var) {
				echo $cnt != 0 ? ", " : "";
				echo "<a href=\"" . get_admin_url() . "edit.php?magazine_cat=" . $var->slug . "&post_type=magazine" . "\">" . $var->name . "</a>";
				++$cnt;
			}
		}
	}
}
add_action('manage_magazine_posts_custom_column', 'show_term_mag_id', 15, 2);

function show_term_news( $defaults ) {
$defaults['news_cat'] = 'お知らせカテゴリ';
return $defaults;
}
add_filter('manage_news_posts_columns', 'show_term_news', 15, 1);

function show_term_news_id($column_name, $id) {
	if( $column_name == 'news_cat' ) {
		$terms = get_the_terms( $id, 'news_cat' );
		$cnt = 0;
		if($terms) {
			foreach($terms as $var) {
				echo $cnt != 0 ? ", " : "";
				echo "<a href=\"" . get_admin_url() . "edit.php?news_cat=" . $var->slug . "&post_type=news" . "\">" . $var->name . "</a>";
				++$cnt;
			}
		}
	}
}
add_action('manage_news_posts_custom_column', 'show_term_news_id', 15, 2);

//追加カスタマイズ
remove_action('wp_head','wp_generator');
remove_action('wp_head', 'wlwmanifest_link');
remove_action('wp_head', 'rsd_link');
remove_action( 'wp_head', 'parent_post_rel_link', 10, 0 );
remove_action( 'wp_head', 'start_post_rel_link', 10, 0 );
remove_action( 'wp_head', 'adjacent_posts_rel_link', 10, 0 );

//管理画面カスタマイズ
add_filter('pre_site_transient_update_core', '__return_zero');
// APIによるバージョンチェックの通信をさせない
remove_action('wp_version_check', 'wp_version_check');
remove_action('admin_init', '_maybe_update_core');
//テーマの更新通知非表示に
remove_action( 'load-update-core.php', 'wp_update_themes' );
add_filter( 'pre_site_transient_update_themes', create_function( '$a', "return null;" ) );

//プラグインのアップデート通知を非表示に
remove_action( 'load-update-core.php', 'wp_update_plugins' );
add_filter( 'pre_site_transient_update_plugins', create_function( '$a', "return null;" ) );

// フッターWordPressリンクを非表示に
function custom_admin_footer() {
	echo 'Copyright © <a href="http://www.jyozaiji.jp/">JYOZAIJI HOURIN-NO-KAI</a> All Rights Reserved';
}
add_filter('admin_footer_text', 'custom_admin_footer');

// ダッシュボードウィジェット非表示
function example_remove_dashboard_widgets() {
	if (!current_user_can('level_10')) { //level10以下のユーザーの場合ウィジェットをunsetする
		global $wp_meta_boxes;
		unset($wp_meta_boxes['dashboard']['normal']['core']['dashboard_right_now']); // 現在の状況
		unset($wp_meta_boxes['dashboard']['normal']['core']['dashboard_recent_comments']); // 最近のコメント
		unset($wp_meta_boxes['dashboard']['normal']['core']['dashboard_incoming_links']); // 被リンク
		unset($wp_meta_boxes['dashboard']['normal']['core']['dashboard_plugins']); // プラグイン
		unset($wp_meta_boxes['dashboard']['side']['core']['dashboard_quick_press']); // クイック投稿
		unset($wp_meta_boxes['dashboard']['side']['core']['dashboard_recent_drafts']); // 最近の下書き
		unset($wp_meta_boxes['dashboard']['side']['core']['dashboard_primary']); // WordPressブログ
		unset($wp_meta_boxes['dashboard']['side']['core']['dashboard_secondary']); // WordPressフォーラム
	}
}
add_action('wp_dashboard_setup', 'example_remove_dashboard_widgets');