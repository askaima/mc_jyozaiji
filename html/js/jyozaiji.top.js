var jyozaijiTop = (function(){

	var $introBlock;
	var $btn;
	var mousewheelevent = 'onwheel' in document ? 'wheel' : 'onmousewheel' in document ? 'mousewheel' : 'DOMMouseScroll';

	function _init()
	{
		$introBlock = $('.introduction');
		$btn = $('.introduction a, .intro a');
		$('html,body').scrollTop(0);

		// イントロアニメーション
		introAnimation();

		// ボタンクリックによるイントロ展開
		$btn.click(introClose);

		// マウススクロースによるイントロ展開
		$(document).on(mousewheelevent, mousewheelCheck);

		// スクロールでページ上部に戻った際、再びイントロ出現
		$(window).scroll(introOpen);

		//GoogleMapの設置
		mapLoad();

		//他ページからのリンク用
		linkPage();

	}

	var introAnimation = function() {
		$introBlock.find('h1').delay(500).animate({opacity:1}, 1000);
		$introBlock.find('p').not('.arrow').css('top', '30px');
		$introBlock.find('p').not('.arrow').delay(1500).animate({opacity:1, top: 0}, 1000, 'easeOutQuad');
		$introBlock.find('.arrow').delay(1500).animate({opacity:1}, 1000);
	}

	var introClose = function() {
		var linkType = $(this).attr('class');

		$introBlock.animate({height:0}, 1500, 'easeOutQuart', function() {
			$('body').css('overflow', 'visible');
			$('#header').removeClass('intro');
			$('html,body').scrollTop(1);

			switch (linkType) {
				case 'link02':
					$('html,body').animate({ scrollTop: $('#read').offset().top }, 500, 'easeOutQuart');
				break;

				case 'link03':
					$('html,body').animate({ scrollTop: $('#look').offset().top }, 1000, 'easeOutQuart');
				break;

				case 'link04':
					$('html,body').animate({ scrollTop: $('#visit').offset().top }, 1500, 'easeOutQuart');
				break;

				case 'link06':
					$('html,body').animate({ scrollTop: $('#access').offset().top }, 2000, 'easeOutQuart');
				break;
			}
		});
	}

	var mousewheelCheck = function(e) {
		if($(window).scrollTop() != 0) {
			return;
		}

		e.preventDefault();
		var delta = e.originalEvent.deltaY ? -(e.originalEvent.deltaY) : e.originalEvent.wheelDelta ? e.originalEvent.wheelDelta : -(e.originalEvent.detail);

		if(delta < 0 && $('body').css('overflow') == 'hidden' && !$introBlock.is(':animated')) {
			introClose();
		}
	}

	var introOpen = function() {
		if($(window).scrollTop() != 0) {
			return;
		}

		if($introBlock.height() == 0) {
			$('#headers').addClass('intro');
			$introBlock.animate({height:'100%'}, 1500, 'easeOutQuart', function() {
				$('body').css('overflow', 'hidden');
			});
		}

	}

	var mapLoad = function() {

		var myLatLng = new google.maps.LatLng(35.639289, 139.650087);

		//マップの設定オプション
		var options = {
			zoom: 16,
			center: myLatLng,
			mapTypeId: google.maps.MapTypeId.ROADMAP,
			scaleControl: true,
			scrollwheel: false
		}

		var map = new google.maps.Map(document.getElementById("map"), options);

		var marker = new google.maps.Marker({
			position: myLatLng,
			map: map
		});
	}


	var linkPage = function() {

		// URLを取得して「?]で分割「&」でも分割
		var url   = location.href;
		params    = url.split("?");
		if(!params[1]) {
			return;
		}
		paramms   = params[1].split("&");
		// パラメータ用の配列を用意
		var paramArray = [];
		// 配列にパラメータを格納
		for ( i = 0; i < paramms.length; i++ ) {
			neet = paramms[i].split("=");
			paramArray.push(neet[0]);
			paramArray[neet[0]] = neet[1];
		}
		var link = paramArray['link'] ;

			switch (link) {
				case 'link02':
					$('.introduction .link02').click();
				break;

				case 'link03':
					$('.introduction .link03').click();
				break;
				case 'link04':
					$('.introduction .link04').click();
				break;

				case 'link06':
					$('.intro .link06').click();
				break;
			}
	}

	return {
		init : _init
	}

}());

$(function(){
	jyozaijiTop.init();
});